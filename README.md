# Simon Quigley's Advent of Code 2016 Solutions

These are my Advent of Code 2016 solutions. Rules before looking at my code:

 1. Please don't steal it. This code was written and developed by Simon Quigley and should NOT be used to get easy solutions.
 1. I will accept any code you would like to contribute AFTER December 25, 2016. Any pull requests before then will be ignored.

If you notice something wrong, either ping me on IRC (tsimonq2 on freenode) or tweet at me (@tsimonq2) on Twitter.
